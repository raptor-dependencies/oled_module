#if !defined(_OLED_Config_h_)
#define _OLED_Config_h_

#include <Config/dev_hardware_SPI.h>
#include <Config/dev_hardware_i2c.h>
#include <Config/Debug.h>
#include <Config/DEV_Config.h>
#include <Config/RPI_sysfs_gpio.h>

#endif

